package main

import (
	"k8s.io/client-go/tools/cache"
	"k8s.io/apimachinery/pkg/fields"
	"time"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"log"
	"fmt"
	"github.com/spf13/viper"
	appsv1 "k8s.io/api/apps/v1"
	"strings"
	"strconv"
)

var podBotheredGeneral = make(map[string]string)

func WatchUsage() {

	if confMap, err := clientset.CoreV1().ConfigMaps("kube-system").Get("pod-bothered-general", metav1.GetOptions{}); err == nil {
		if confMap.Data != nil {
			podBotheredGeneral = confMap.Data
		}
	} else {
		log.Printf("Error reading the config pod-bothered-general: %s", err.Error())
	}

	go func() {
		for range time.Tick(time.Minute) {
			if confMap, err := clientset.CoreV1().ConfigMaps("kube-system").Get("pod-bothered-general", metav1.GetOptions{}); err == nil {
				confMap.Data = podBotheredGeneral
				if _, err := clientset.CoreV1().ConfigMaps("kube-system").Update(confMap); err != nil {
					log.Printf("Error updating confMap for podsBotheredGeneral: %s", err.Error())
				}
			} else {
				if _, err := clientset.CoreV1().ConfigMaps("kube-system").Create(&v1.ConfigMap{
					ObjectMeta: metav1.ObjectMeta{
						Name: "pod-bothered-general",
					},
					Data: podBotheredGeneral,
				}); err != nil {
					log.Printf("Error submiting confMap for podsBotheredGeneral: %s", err.Error())
				}
			}

		}
	}()

	// PODS
	lw_pods := cache.NewListWatchFromClient(
		clientset.CoreV1().RESTClient(),
		"pods",
		v1.NamespaceAll,
		fields.Everything())

	_, controller_pods := cache.NewInformer(
		lw_pods,
		&v1.Pod{},
		botherSampling,
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				pod, ok := obj.(*v1.Pod)
				if !ok {
					log.Printf("Expected Pod but other received %#v", obj)
					return
				}

				if len(pod.GetOwnerReferences()) > 0 {
					return
				}

				checkObj(&pod.ObjectMeta, "Pod")
			},
			DeleteFunc: func(obj interface{}) {
				pod, ok := obj.(*v1.Pod)
				if !ok {
					log.Printf("Expected Pod but other received %#v", obj)
					return
				}
				delete(podBotheredGeneral, string(pod.UID))
			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				pod, ok := newObj.(*v1.Pod)
				if !ok {
					log.Printf("Expected Pod but other received %#v", newObj)
					return
				}
				if pod.Status.Phase == v1.PodRunning {
					checkObj(&pod.ObjectMeta, "Pod")
				}
			},
		},
	)

	// RS
	lw_replicasets := cache.NewListWatchFromClient(
		clientset.AppsV1().RESTClient(),
		"replicasets",
		v1.NamespaceAll,
		fields.Everything())

	_, controller_replicasets := cache.NewInformer(
		lw_replicasets,
		&appsv1.ReplicaSet{},
		botherSampling,
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				rs, ok := obj.(*appsv1.ReplicaSet)
				if !ok {
					log.Printf("Expected ReplicaSet but other received %#v", obj)
					return
				}

				if len(rs.GetOwnerReferences()) > 0 {
					return
				}

				checkObj(&rs.ObjectMeta, "ReplicaSet")

			},
			DeleteFunc: func(obj interface{}) {
				rs, ok := obj.(*appsv1.ReplicaSet)
				if !ok {
					log.Printf("Expected ReplicaSet but other received %#v", obj)
					return
				}
				delete(podBotheredGeneral, string(rs.UID))
			},
		},
	)

	// Deployments
	lw_deployments := cache.NewListWatchFromClient(
		clientset.AppsV1().RESTClient(),
		"deployments",
		v1.NamespaceAll,
		fields.Everything())

	_, controller_deployments := cache.NewInformer(
		lw_deployments,
		&appsv1.Deployment{},
		botherSampling,
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				dp, ok := obj.(*appsv1.Deployment)
				if !ok {
					log.Printf("Expected Deployment but other received %#v", obj)
					return
				}

				checkObj(&dp.ObjectMeta, "Deployment")

			},
			DeleteFunc: func(obj interface{}) {
				dp, ok := obj.(*appsv1.Deployment)
				if !ok {
					log.Printf("Expected Deployment but other received %#v", obj)
					return
				}
				delete(podBotheredGeneral, string(dp.UID))
			},
		},
	)

	stop := make(chan struct{})
	go controller_pods.Run(stop)
	go controller_replicasets.Run(stop)
	go controller_deployments.Run(stop)

	// Wait forever
	select {}
}

func checkObj(meta *metav1.ObjectMeta, kind string) {
	if meta.GetCreationTimestamp().UTC().After(time.Now().Add(time.Duration(-time.Hour*24*14))) {
		return
	}

	for _, ns := range viper.GetStringSlice("general_exceptions") {
		if meta.Namespace == ns {
			return
		}
	}

	if botheredTimeStr, ok := podBotheredGeneral[string(meta.UID)]; ok {
		var botheredTime time.Time
		if spl := strings.Split(botheredTimeStr, "|"); len(spl) > 1 {
			if err := botheredTime.UnmarshalText([]byte(spl[0])); err == nil {
				if botheredTime.After(time.Now().Add(time.Duration(-24 * time.Hour + time.Minute))) {
					//log.Printf("Not bothering %s too soon", meta.Name)
					return
				}
			} else {
				log.Printf("Error parsing bother time %s: %s", spl[0], err.Error())
			}
		}
	}

	userEmails := []string{}
	if userBindings, err := clientset.RbacV1().RoleBindings(meta.Namespace).Get("nautilus-admin", metav1.GetOptions{}); err == nil {
		if len(userBindings.Subjects) > 0 {
			for _, userBinding := range userBindings.Subjects {
				if user, err := GetUser(userBinding.Name); err == nil {
					userEmails = append(userEmails, fmt.Sprintf("%s <%s>", user.Spec.Name, user.Spec.Email))
				} else {
					log.Printf("Error getting admins to send emails: %s", err.Error())
				}
			}

		} else {
			log.Printf("No admins found in namespace: %s", meta.Namespace)
		}
	}
	if userBindings, err := clientset.RbacV1().RoleBindings(meta.Namespace).Get("nautilus-user", metav1.GetOptions{}); err == nil {
		if len(userBindings.Subjects) > 0 {
			for _, userBinding := range userBindings.Subjects {
				if user, err := GetUser(userBinding.Name); err == nil {
					userEmails = append(userEmails, fmt.Sprintf("%s <%s>", user.Spec.Name, user.Spec.Email))
				} else {
					log.Printf("Error getting users to send emails: %s", err.Error())
				}
			}

		} else {
			log.Printf("No users found in namespace: %s", meta.Namespace)
		}
	}

	if len(userEmails) > 0 {
		timesBothered := 0
		if spl := strings.Split(podBotheredGeneral[string(meta.UID)], "|"); len(spl) > 1 {
			if i, err := strconv.Atoi(spl[1]); err == nil {
				timesBothered = i
			}
		}

		if timesBothered >= 3 {
			clearObject(userEmails, meta, kind)
		} else {
			if botherTimeBytes, err := time.Now().MarshalText(); err == nil {
				podBotheredGeneral[string(meta.UID)] = fmt.Sprintf("%s|%d", botherTimeBytes, timesBothered+1)
			}
			botherUsersAboutRunTime(userEmails, meta, kind)
		}
	}
}

func botherUsersAboutRunTime(destination []string, meta *metav1.ObjectMeta, kind string) {
	destination = append(destination, "Dmitry Mishin <dmishin@ucsd.edu>")
	destination = append(destination, "John Graham <jjgraham@ucsd.edu>")
	r := NewMailRequest(destination, "Nautilus cluster: object runtime limit")

	err := r.parseTemplate("templates/usagemail.tmpl", "usagemail.tmpl", map[string]interface{}{
		"meta": meta,
		"kind":	kind,
	})
	if err != nil {
		log.Printf("Error parsing the usage email template: %s", err.Error())
	}
	log.Printf("Bothering user %s about %s object %s:%s started %s", r.to, kind, meta.Namespace, meta.Name, meta.CreationTimestamp)
	if err := r.sendMail(); err != nil {
		log.Printf("Failed to send the email to %s : %s\n", r.to, err.Error())
	} else {
		log.Printf("Email has been sent to %s\n", r.to)
	}
}

func clearObject(destination []string, meta *metav1.ObjectMeta, kind string) {
	switch kind {
	case "Pod":
		clientset.CoreV1().Pods(meta.Namespace).Delete(meta.Name, &metav1.DeleteOptions{})
	case "ReplicaSet":
		clientset.AppsV1().ReplicaSets(meta.Namespace).Delete(meta.Name, &metav1.DeleteOptions{})
	case "Deployment":
		clientset.AppsV1().Deployments(meta.Namespace).Delete(meta.Name, &metav1.DeleteOptions{})
	}

	destination = append(destination, "Dmitry Mishin <dmishin@ucsd.edu>")
	destination = append(destination, "John Graham <jjgraham@ucsd.edu>")
	r := NewMailRequest(destination, "Nautilus cluster: object runtime limit")

	err := r.parseTemplate("templates/purgedmail.tmpl", "purgedmail.tmpl", map[string]interface{}{
		"meta": meta,
		"kind":	kind,
	})
	if err != nil {
		log.Printf("Error parsing the purged email template: %s", err.Error())
	}
	log.Printf("Notifying user %s about purged %s object %s:%s started %s", r.to, kind, meta.Namespace, meta.Name, meta.CreationTimestamp)
	if err := r.sendMail(); err != nil {
		log.Printf("Failed to send the email to %s : %s\n", r.to, err.Error())
	} else {
		log.Printf("Email has been sent to %s\n", r.to)
	}
}
