package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strings"
	"time"

	nautilusapi "gitlab.com/ucsd-prp/k8s_portal/pkg/apis/optiputer.net/v1alpha1"
	v1 "k8s.io/api/core/v1"

	rbacv1 "k8s.io/api/rbac/v1"
	apiextcs "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
)

type NamespaceUserBinding struct {
	Namespace v1.Namespace
	ConfigMap v1.ConfigMap
}

func GetCrd() {
	k8sconfig, err := rest.InClusterConfig()
	if err != nil {
		log.Fatal("Failed to do inclusterconfig: " + err.Error())
		return
	}

	crdclientset, err := apiextcs.NewForConfig(k8sconfig)
	if err != nil {
		panic(err.Error())
	}

	if err := nautilusapi.CreateCRD(crdclientset); err != nil {
		log.Printf("Error creating CRD: %s", err.Error())
	}

	// Wait for the CRD to be created before we use it (only needed if its a new one)
	time.Sleep(3 * time.Second)

	_, controller := cache.NewInformer(
		crdclient.NewListWatch(),
		&nautilusapi.PRPUser{},
		time.Minute*5,
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				user, ok := obj.(*nautilusapi.PRPUser)
				if !ok {
					log.Printf("Expected PRPUser but other received %#v", obj)
					return
				}

				updateClusterUserPrivileges(user)
			},
			DeleteFunc: func(obj interface{}) {
				user, ok := obj.(*nautilusapi.PRPUser)
				if !ok {
					log.Printf("Expected PRPUser but other received %#v", obj)
					return
				}

				if rb, err := clientset.Rbac().ClusterRoleBindings().Get("nautilus-cluster-user", metav1.GetOptions{}); err == nil {
					allSubjects := []rbacv1.Subject{} // to filter the user, in case we need to delete one

					found := false
					for _, subj := range rb.Subjects {
						if subj.Name == user.Spec.UserID {
							found = true
						} else {
							allSubjects = append(allSubjects, subj)
						}
					}
					if found {
						rb.Subjects = allSubjects
						if _, err := clientset.Rbac().ClusterRoleBindings().Update(rb); err != nil {
							log.Printf("Error updating user %s: %s", user.Name, err.Error())
						}
					}
				}
			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				oldUser, ok := oldObj.(*nautilusapi.PRPUser)
				if !ok {
					log.Printf("Expected PRPUser but other received %#v", oldObj)
					return
				}
				newUser, ok := newObj.(*nautilusapi.PRPUser)
				if !ok {
					log.Printf("Expected PRPUser but other received %#v", newObj)
					return
				}
				if oldUser.Spec.Role != newUser.Spec.Role {
					updateClusterUserPrivileges(newUser)
				}
			},
		},
	)

	stop := make(chan struct{})
	go controller.Run(stop)

	// Wait forever
	select {}
}

func updateClusterUserPrivileges(user *nautilusapi.PRPUser) error {
	allSubjects := []rbacv1.Subject{} // to filter the user, in case we need to delete one

	if rb, err := clientset.Rbac().ClusterRoleBindings().Get("nautilus-cluster-user", metav1.GetOptions{}); err == nil {
		found := false
		for _, subj := range rb.Subjects {
			if subj.Name == user.Spec.UserID {
				found = true
			} else {
				allSubjects = append(allSubjects, subj)
			}
		}
		if !user.IsGuest() && !found {
			rb.Subjects = append(rb.Subjects, rbacv1.Subject{
				Kind:     "User",
				APIGroup: "rbac.authorization.k8s.io",
				Name:     user.Spec.UserID})
			if _, err := clientset.Rbac().ClusterRoleBindings().Update(rb); err != nil {
				return err
			}
		} else if user.IsGuest() && found {
			rb.Subjects = allSubjects
			if _, err := clientset.Rbac().ClusterRoleBindings().Update(rb); err != nil {
				return err
			}
		}
	} else if !user.IsGuest() {
		if _, err := clientset.Rbac().ClusterRoleBindings().Create(&rbacv1.ClusterRoleBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name: "nautilus-cluster-user",
			},
			RoleRef: rbacv1.RoleRef{
				APIGroup: "rbac.authorization.k8s.io",
				Kind:     "ClusterRole",
				Name:     "nautilus-cluster-user",
			},
			Subjects: []rbacv1.Subject{{
				Kind:     "User",
				APIGroup: "rbac.authorization.k8s.io",
				Name:     user.Spec.UserID}},
		}); err != nil {
			return err
		}
	}
	return nil
}

// Process the /profile path
func ProfileHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method != "GET" {
		return
	}

	session, err := filestore.Get(r, "prp-session")
	if err != nil {
		log.Printf("Error getting the session: %s", err.Error())
	}

	if session.IsNew || session.Values["userid"] == nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	user, err := GetUser(session.Values["userid"].(string))
	if err != nil {
		session.AddFlash(fmt.Sprintf("Unexpected error: %s", err.Error()))
		session.Save(r, w)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	userclientset, err := user.GetUserClientset()
	if err != nil {
		session.AddFlash(fmt.Sprintf("Unexpected error: %s", err.Error()))
		session.Save(r, w)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if strings.ToLower(user.Spec.Role) != "admin" {
		session.AddFlash("Only admins can manage namespaces")
		session.Save(r, w)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	// User requested to add another user to namespace
	addUserName := r.URL.Query().Get("addusername")
	addUserNs := r.URL.Query().Get("adduserns")

	if addUserName != "" && addUserNs != "" {
		requser, err := GetUser(addUserName)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("Unexpected error %s", err.Error())))
			return
		}

		if err := createNsRoleBinding(addUserNs, requser, userclientset); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("Error adding user to namespace %s", err.Error())))
			return
		} else {
			w.WriteHeader(204)
			return
		}
	}

	// User requested to delete another user from namespace
	delUserName := r.URL.Query().Get("delusername")
	delUserNs := r.URL.Query().Get("deluserns")

	if delUserName != "" && delUserNs != "" {
		requser, err := GetUser(delUserName)
		if err != nil {
			session.AddFlash(fmt.Sprintf("Unexpected error: %s", err.Error()))
			session.Save(r, w)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		if err := delNsRoleBinding(delUserNs, requser, userclientset); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("Error deleting user from namespace %s", err.Error())))
			return
		} else {
			w.WriteHeader(204)
			return
		}
	}

	var queryObject = r.URL.Query().Get("query")
	if queryObject != "" {
		switch queryObject {
		case "namespaces":
			nsList := []NamespaceUserBinding{}

			isClusterAdmin := false
			if crblist, err := clientset.RbacV1().ClusterRoleBindings().List(metav1.ListOptions{}); err == nil {
				for _, crb := range crblist.Items {
					for _, subj := range crb.Subjects {
						if subj.Kind == rbacv1.UserKind && subj.Name == user.Spec.UserID && crb.RoleRef.Name == "cluster-admin" {
							isClusterAdmin = true
						}
					}
				}
			}

			nsMap := map[string]v1.Namespace{}
			if isClusterAdmin { // sees all namespaces
				if allns, err := clientset.CoreV1().Namespaces().List(metav1.ListOptions{}); err == nil {
					for _, ns := range allns.Items {
						nsMap[ns.Name] = ns
					}
				} else {
					fmt.Printf("Error getting admin NS list %s", err.Error())
				}
			} else {
				if rblist, err := clientset.RbacV1().RoleBindings("").List(metav1.ListOptions{}); err == nil {
					for _, rb := range rblist.Items {
						for _, subj := range rb.Subjects {
							if subj.Kind == rbacv1.UserKind && subj.Name == user.Spec.UserID {
								if ns, err := clientset.CoreV1().Namespaces().Get(rb.Namespace, metav1.GetOptions{}); err == nil {
									nsMap[rb.Namespace] = *ns
								}
							}
						}
					}
				}

			}

			for _, ns := range nsMap {
				nsBind := NamespaceUserBinding{Namespace: ns}
				if metaConfig, err := clientset.CoreV1().ConfigMaps(ns.GetName()).Get("meta", metav1.GetOptions{}); err == nil {
					nsBind.ConfigMap = *metaConfig
				}
				nsList = append(nsList, nsBind)
			}


			if nsJson, err := json.Marshal(nsList); err == nil {
				w.Write(nsJson)
				return
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("Error getting nsmespaces: %s", err.Error())))
				return
			}
		}
	}

	nsVars := buildIndexTemplateVars(session, w, r)

	t, err := template.New("layout.tmpl").ParseFiles("templates/layout.tmpl", "templates/profile.tmpl")
	if err != nil {
		w.Write([]byte(err.Error()))
	} else {
		err = t.ExecuteTemplate(w, "layout.tmpl", nsVars)
		if err != nil {
			w.Write([]byte(err.Error()))
		}
	}
}

func MakeNsHandler(w http.ResponseWriter, r *http.Request) {

	session, err := filestore.Get(r, "prp-session")
	if err != nil {
		log.Printf("Error getting the session: %s", err.Error())
	}

	if session.IsNew || session.Values["userid"] == nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error accessing new namespace: %s", err.Error())))
		return
	}

	user, err := GetUser(session.Values["userid"].(string))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("%s", err.Error())))
		return
	}

	userclientset, err := user.GetUserClientset()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("%s", err.Error())))
		return
	}

	if strings.ToLower(user.Spec.Role) != "admin" {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Only admins can manage namespaces"))
		return
	}

	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	switch r.Method {
	case "POST":
		createNsName := r.PostFormValue("name")
		if createNsName != "" {
			if ns, err := clientset.Core().Namespaces().List(metav1.ListOptions{
				FieldSelector: fields.OneTermEqualSelector("metadata.name", createNsName).String()}); len(ns.Items) == 0 && err == nil {
				if _, err := clientset.Core().Namespaces().Create(&v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: createNsName}}); err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					w.Write([]byte(fmt.Sprintf("Error creating the namespace: %s", err.Error())))
					return
				} else {
					if _, err := createNsLimits(createNsName); err != nil {
						log.Printf("Error creating limits: %s", err.Error())
					}

					if err := createNsRoleBinding(createNsName, user, clientset); err != nil {
						log.Printf("Error creating userbinding %s", err.Error())
					}
				}

			} else {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("The namespace %s already exists or error %v", createNsName, err)))
				return
			}

			if nsNew, err := clientset.Core().Namespaces().Get(createNsName, metav1.GetOptions{}); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("Error accessing new namespace: %s", err.Error())))
				return
			} else {
				nsBind := NamespaceUserBinding{Namespace: *nsNew}

				dataMap := map[string]string{"PI": r.PostFormValue("pi"), "Grant": r.PostFormValue("grant")}
				if confMap, err := userclientset.Core().ConfigMaps(createNsName).Create(&v1.ConfigMap{
					ObjectMeta: metav1.ObjectMeta{
						Name: "meta",
					},
					Data: dataMap,
				}); err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					w.Write([]byte(err.Error()))
					return
				} else {
					nsBind.ConfigMap = *confMap
				}

				if nsJson, err := json.Marshal(nsBind); err == nil {
					w.Write(nsJson)
					return
				} else {
					w.WriteHeader(http.StatusInternalServerError)
					w.Write([]byte(fmt.Sprintf("Error accessing new namespace: %s", err.Error())))
					return
				}
			}

		}

	case "DELETE":
		delNsName := r.FormValue("name")
		if delNsName != "" {
			if strings.HasPrefix(delNsName, "kube-") || delNsName == "default" {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("Can't delete standard namespace %s", delNsName)))
				return
			} else {
				if err := userclientset.Core().Namespaces().Delete(delNsName, &metav1.DeleteOptions{}); err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					w.Write([]byte(fmt.Sprintf("Error deleting the namespace: %s", err.Error())))
					return
				} else {
					w.WriteHeader(204)
					return
				}
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Error deleting the namespace: name is empty"))
			return
		}
	}

}

func NsMetaHandler(w http.ResponseWriter, r *http.Request) {
	session, err := filestore.Get(r, "prp-session")
	if err != nil {
		log.Printf("Error getting the session: %s", err.Error())
	}

	if session.IsNew || session.Values["userid"] == nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	user, err := GetUser(session.Values["userid"].(string))
	if err != nil {
		session.AddFlash(fmt.Sprintf("Unexpected error: %s", err.Error()))
		session.Save(r, w)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	userclientset, err := user.GetUserClientset()
	if err != nil {
		session.AddFlash(fmt.Sprintf("Unexpected error: %s", err.Error()))
		session.Save(r, w)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if strings.ToLower(user.Spec.Role) != "admin" {
		session.AddFlash("Only admins can manage namespaces")
		session.Save(r, w)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	updateNsName := r.PostFormValue("Namespace[metadata][name]")
	if updateNsName != "" {
		if confMap, err := userclientset.Core().ConfigMaps(updateNsName).Get("meta", metav1.GetOptions{}); err == nil {
			confMap.Data["PI"] = r.PostFormValue("ConfigMap[data][Grant]")
			confMap.Data["Grant"] = r.PostFormValue("ConfigMap[data][PI]")
			if _, err := userclientset.Core().ConfigMaps(updateNsName).Update(confMap); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}
		} else {
			dataMap := map[string]string{"PI": r.PostFormValue("ConfigMap[data][PI]"), "Grant": r.PostFormValue("ConfigMap[data][Grant]")}
			if _, err := userclientset.Core().ConfigMaps(updateNsName).Create(&v1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "meta",
				},
				Data: dataMap,
			}); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}
		}
	}
}

// Creates a new rolebinding
func createNsRoleBinding(nsName string, user *nautilusapi.PRPUser, userclientset *kubernetes.Clientset) error {
	if user.IsGuest() {
		return fmt.Errorf("Please verify the guest user to add to the namespace.")
	}

	if rb, err := userclientset.Rbac().RoleBindings(nsName).Get("psp:nautilus-user", metav1.GetOptions{}); err == nil {
		found := false
		for _, subj := range rb.Subjects {
			if subj.Name == user.Spec.UserID {
				found = true
			}
		}
		if !found {
			rb.Subjects = append(rb.Subjects, rbacv1.Subject{
				Kind:     "User",
				APIGroup: "rbac.authorization.k8s.io",
				Name:     user.Spec.UserID})
			if _, err := userclientset.Rbac().RoleBindings(nsName).Update(rb); err != nil {
				return err
			}
		}
	} else {
		if _, err := userclientset.Rbac().RoleBindings(nsName).Create(&rbacv1.RoleBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name: "psp:nautilus-user",
			},
			RoleRef: rbacv1.RoleRef{
				APIGroup: "rbac.authorization.k8s.io",
				Kind:     "ClusterRole",
				Name:     "psp:nautilus-user",
			},
			Subjects: []rbacv1.Subject{
				{
					Kind: "ServiceAccount",
					Name: "default"},
				{
					Kind:     "User",
					APIGroup: "rbac.authorization.k8s.io",
					Name:     user.Spec.UserID},
			},
		}); err != nil {
			return err
		}
	}

	if user.Spec.Role == "admin" {
		if rb, err := userclientset.Rbac().RoleBindings(nsName).Get("nautilus-admin-ext", metav1.GetOptions{}); err == nil {
			found := false
			for _, subj := range rb.Subjects {
				if subj.Name == user.Spec.UserID {
					found = true
				}
			}
			if !found {
				rb.Subjects = append(rb.Subjects, rbacv1.Subject{
					Kind:     "User",
					APIGroup: "rbac.authorization.k8s.io",
					Name:     user.Spec.UserID})
				if _, err := userclientset.Rbac().RoleBindings(nsName).Update(rb); err != nil {
					return err
				}
			}
		} else {
			if _, err := userclientset.Rbac().RoleBindings(nsName).Create(&rbacv1.RoleBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name: "nautilus-admin-ext",
				},
				RoleRef: rbacv1.RoleRef{
					APIGroup: "rbac.authorization.k8s.io",
					Kind:     "ClusterRole",
					Name:     "nautilus-admin",
				},
				Subjects: []rbacv1.Subject{{
					Kind:     "User",
					APIGroup: "rbac.authorization.k8s.io",
					Name:     user.Spec.UserID}},
			}); err != nil {
				return err
			}
		}
	}

	if rb, err := userclientset.Rbac().RoleBindings(nsName).Get("nautilus-"+user.Spec.Role, metav1.GetOptions{}); err == nil {
		found := false
		for _, subj := range rb.Subjects {
			if subj.Name == user.Spec.UserID {
				found = true
			}
		}
		if !found {
			rb.Subjects = append(rb.Subjects, rbacv1.Subject{
				Kind:     "User",
				APIGroup: "rbac.authorization.k8s.io",
				Name:     user.Spec.UserID})
			_, err := userclientset.Rbac().RoleBindings(nsName).Update(rb)
			return err
		}
	} else {
		clusterRoleName := ""
		switch user.Spec.Role {
		case "user":
			clusterRoleName = "edit"
		case "admin":
			clusterRoleName = "admin"
		}
		_, err := userclientset.Rbac().RoleBindings(nsName).Create(&rbacv1.RoleBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name: "nautilus-" + user.Spec.Role,
			},
			RoleRef: rbacv1.RoleRef{
				APIGroup: "rbac.authorization.k8s.io",
				Kind:     "ClusterRole",
				Name:     clusterRoleName,
			},
			Subjects: []rbacv1.Subject{{
				Kind:     "User",
				APIGroup: "rbac.authorization.k8s.io",
				Name:     user.Spec.UserID}},
		})
		return err
	}
	return nil

}

// Deletes a rolebinding
func delNsRoleBinding(nsName string, user *nautilusapi.PRPUser, userclientset *kubernetes.Clientset) error {
	if rb, err := userclientset.Rbac().RoleBindings(nsName).Get("psp:nautilus-user", metav1.GetOptions{}); err == nil {
		allSubjects := []rbacv1.Subject{} // to filter the user, in case we need to delete one
		found := false
		for _, subj := range rb.Subjects {
			if subj.Name == user.Spec.UserID {
				found = true
			} else {
				allSubjects = append(allSubjects, subj)
			}
		}
		if found {
			if len(allSubjects) == 0 {
				if err := userclientset.Rbac().RoleBindings(nsName).Delete(rb.GetName(), &metav1.DeleteOptions{}); err != nil {
					return err
				}
			} else {
				rb.Subjects = allSubjects
				if _, err := userclientset.Rbac().RoleBindings(nsName).Update(rb); err != nil {
					return err
				}
			}
		}
	}

	if user.Spec.Role == "admin" {
		if rb, err := userclientset.Rbac().RoleBindings(nsName).Get("nautilus-admin-ext", metav1.GetOptions{}); err == nil {
			allSubjects := []rbacv1.Subject{} // to filter the user, in case we need to delete one
			found := false
			for _, subj := range rb.Subjects {
				if subj.Name == user.Spec.UserID {
					found = true
				} else {
					allSubjects = append(allSubjects, subj)
				}
			}
			if found {
				if len(allSubjects) == 0 {
					if err := userclientset.Rbac().RoleBindings(nsName).Delete(rb.GetName(), &metav1.DeleteOptions{}); err != nil {
						return err
					}
				} else {
					rb.Subjects = allSubjects
					if _, err := userclientset.Rbac().RoleBindings(nsName).Update(rb); err != nil {
						return err
					}
				}
			}
		}
	}

	if rb, err := userclientset.Rbac().RoleBindings(nsName).Get("nautilus-"+user.Spec.Role, metav1.GetOptions{}); err == nil {
		allSubjects := []rbacv1.Subject{} // to filter the user, in case we need to delete one
		found := false
		for _, subj := range rb.Subjects {
			if subj.Name == user.Spec.UserID {
				found = true
			} else {
				allSubjects = append(allSubjects, subj)
			}
		}
		if found {
			if len(allSubjects) == 0 {
				if err := userclientset.Rbac().RoleBindings(nsName).Delete(rb.GetName(), &metav1.DeleteOptions{}); err != nil {
					return err
				}
			} else {
				rb.Subjects = allSubjects
				_, err := userclientset.Rbac().RoleBindings(nsName).Update(rb)
				return err
			}
		}
	}
	return nil
}

// Creates a namespace default limits
func createNsLimits(ns string) (*v1.LimitRange, error) {
	return clientset.Core().LimitRanges(ns).Create(&v1.LimitRange{
		ObjectMeta: metav1.ObjectMeta{Name: ns + "-mem"},
		Spec: v1.LimitRangeSpec{
			Limits: []v1.LimitRangeItem{
				{
					Type: v1.LimitTypeContainer,
					Default: map[v1.ResourceName]resource.Quantity{
						v1.ResourceMemory: resource.MustParse("4Gi"),
					},
					DefaultRequest: map[v1.ResourceName]resource.Quantity{
						v1.ResourceMemory: resource.MustParse("256Mi"),
					},
				},
			},
		},
	})
}
