{{define "body"}}
<div class="container">
  <div class="jumbotron">
    <div id="jsGrid"></div>
  </div>
</div>
{{end}}

{{define "page_js"}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.js" integrity="sha256-zs4Ql/EnwyWVY+mTbGS2WIMLdfYGtQOhkeUtOawKZVY=" crossorigin="anonymous"></script>
<script type="text/javascript" src="media/jsgrid.min.js"></script>
<script language="JavaScript">
function viewns(ns) {
  $.ajax({
    url: '/users',
    data: { format: "json", action: "namespace", namespace: ns },
    dataType: "json",
    success: function(result, textStatus, xhr){

      var usersStr = "";
      if(result.users) {
        usersStr = [
          '<b>Users: </b>',
          result.users.map(function(item) {
            return "<span class='roleref'><i class='fa fa-trash' style='color:red; cursor: pointer;' title='Remove user from namespace' onclick='deluser(\""+item.spec.UserID+"\", \""+ns+"\")'></i> "+item.spec.Name+" &lt;"+"<a href='mailto:"+item.spec.Email+"'>"+item.spec.Email+"</a>&gt;</span>"
          }).join(' '),
          '<br/>',
        ].join('')
      }

      var adminsStr = "";
      if(result.admins) {
        adminsStr = [
          '<b>Admins: </b>',
          result.admins.map(function(item) {
            return "<span class='roleref'><i class='fa fa-trash' style='color:red; cursor: pointer;' title='Remove admin from namespace' onclick='deluser(\""+item.spec.UserID+"\", \""+ns+"\")'></i> "+item.spec.Name+" &lt;"+"<a href='mailto:"+item.spec.Email+"'>"+item.spec.Email+"</a>&gt;</span>"
          }).join(' '),
        ].join('')
      }

      if (adminsStr + usersStr == "") {
        usersStr = "No users defined";
      }

      vex.dialog.alert({ unsafeMessage: [
        usersStr,
        adminsStr,
      ].join('')});
    },
    error: function(xhr, text){
      vex.dialog.alert(text)
    },
  });
}

function adduser(ns) {
  vex.dialog.open({
    message: 'Select user to add to the namespace.',
    input: [
      '<div class="vex-custom-field-wrapper">',
      '<label for="user">User</label>',
      '<div class="vex-custom-input-wrapper">',
      '<input name="user" type="text" class="autosuggest"/>',
      '</div>',
      '</div>'
    ].join(''),
    callback: function (data) {
      if (!data) {
        return console.log('Cancelled')
      }
      $.ajax({url: "/profile", data: {addusername: data.user, adduserns:ns}}).done(function(response) {
        vex.dialog.alert({message: "Added a user to namespace "+ns+"."});
      }).fail(function(jqXHR, textStatus, errorThrown) {
        vex.dialog.alert({message: jqXHR.responseText});
      });
    }
  })
  var xhr;
  $('input.autosuggest').autoComplete({
    minChars: 2,
    source: function(term, suggest) {
        try { xhr.abort(); } catch(e){}
        xhr = $.getJSON('/users', { format:"json", action:"autocomplete", term: term }, function(data){ suggest(data); });
    },
    renderItem: function (item, search) {
        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      return '<div class="autocomplete-suggestion" data-val="' + item.value + '">'+ item.label.replace(re, "<b>$1</b>") + '</div>';
    }
  });
}

function deluser(user, ns) { 
  $.ajax({url: "/profile", data: {delusername: user, deluserns:ns}}).done(function(response) {
    vex.closeAll();
    vex.dialog.alert({message: "Deleted a user from namespace "+ns+"."});
  });
} 

$("#jsGrid").jsGrid({
    width: "100%",
    controller: {
      loadData: function(filter) {
        var d = $.Deferred();
        $.ajax({url: "/profile", dataType: "json", data: {"query": "namespaces"}}).done(function(response) {
          d.resolve(response);
        });
        return d.promise();
      },
      insertItem: function(item) {
        var d = $.Deferred();
        $.ajax({
          url: '/mkns',
          type: "POST",
          data: { "name": item.Namespace.metadata.name,  "pi": item.ConfigMap.data.PI, "grant": item.ConfigMap.data.Grant},
          dataType: "json",
          success: function(result, textStatus, xhr){
            // vex.dialog.alert({message: "Created the namespace "+item.Namespace.metadata.name+"."});
          },
          error: function(xhr, textStatus, errorThrown){
            vex.dialog.alert("Request failed: "+xhr.responseText);
          }
        }).done(function(response) {
          d.resolve(response);
        }).fail(function(jqXHR, textStatus, errorThrown) {
          d.reject(jqXHR.responseText);
        });
        return d.promise();
      },
      onItemInserted: function(args) {
        // console.debug("Sorting");
        $("#jsGrid").jsGrid("refresh");
        $("#jsGrid").jsGrid("sort", 0, "asc");
      },
      updateItem: function(item) {
          return $.ajax({
              type: "POST",
              url: "/nsMeta",
              data: item
          });
      },      
      deleteItem: function(item, value) {
        return $.ajax({
          url: '/mkns?' + $.param({ "name": item.Namespace.metadata.name }),
          type: 'DELETE',
          success: function(result, textStatus, xhr){
            vex.dialog.alert({message: "Deleted the namespace "+item.Namespace.metadata.name+"."});
          },
          error: function(xhr, textStatus, errorThrown){
            vex.dialog.alert("Request failed: "+xhr.responseText);
          }
        });
      },      
    },
    autoload: false,
    sorting: true,
    selecting: false,
    inserting: true,
    editing: true,
    insertRowLocation: "top",
    rowClick: function(args) {
      if(args.event.target.tagName == "TD" && args.event.target.cellIndex != 0) {
          this.editItem($(args.event.target).closest("tr"));
      } else if(args.event.target.tagName == "A") {
        viewns(args.event.target.textContent);
      }
    },
    fields: [
        { 
          title: "Namespace", 
          name: "Namespace.metadata.name", 
          width: "55",
          editing: false,
          type: "text",
          itemTemplate: function(value, item) {
           return '<a class="nstitle">'+value+'</a> '+
              '<button type="button" class="addUserBtn" title="Add user" onclick="adduser(\''+value+'\')"><i class="fa fa-address-book-o" aria-hidden="true"></i></button>'
          }, 
        },
        { title: "Grant", name: "ConfigMap.data.Grant", width: "35", editing: true, type: "text"},
        { title: "PI", name: "ConfigMap.data.PI", width: "35", editing: true, type: "text"},
        { name: "", type: 'control',  width: "10",  visible: true, inserting: true, editing: false, filtering: false, itemTemplate: function(value, item) {
            var $result = $([]); 
            $result = $result.add(this._createDeleteButton(item));
            return $result;
          } 
        }
    ]
});

$("#jsGrid").jsGrid("loadData").done(function() {
  $("#jsGrid").jsGrid("sort", 0);
});


</script>
{{end}}

{{define "page_css"}}

<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.css" integrity="sha256-MFTTStFZmJT7CqZBPyRVaJtI2P9ovNBbwmr0/KErfEc=" crossorigin="anonymous" />
<style>
span.roleref {
  background: white;
  padding: 3px;
  border-radius: 3px;
  font-size: 0.6em;
}

span.ialert {
  color: red;
}

.nstitle {
    color: blue !important;
    text-decoration: underline !important;
    cursor: pointer;
}

.addUserBtn {
  width: 25px; 
  height: 25px; 
  border: none; 
  cursor: pointer; 
  padding: 0px;
}
</style>
{{end}}
